# Equipement sportif nantais

<div align="center">
<img width="500" height="400" src="Equipement_sportif.jpg">
</div>

## Description du projet

Application web réalisée avec Python en DUT INFO 2 à l'IUT de Nantes dans le cadre du module "Technologies de production logiciels" durant l'année 2017-2018 avec un groupe de deux personnes. \
Elle permet de trouver un équipement sportif dans la région nantaise le plus proche de chez soi afin d'en pratiquer.

## Acteurs

### Réalisateurs

Ce projet a été réalisé par un groupe de deux étudiants de l'IUT de Nantes :
- Théo MAHAUDA : theo.mahauda@etu.univ-nantes.fr ;
- Clara DEGREZ : clara.degrez@etu.univ-nantes.fr.

### Encadrants

Ce projet fut encadré par un enseignant de l'IUT de Nantes :
- Jean-François BERDJUGIN – jean-francois.berdjugin@univ-nantes.fr.

## Organisation

Ce projet a été agit au sein de l'université de Nantes dans le cadre du module "Technologies de production logiciels" du DUT INFO 2.

## Date de réalisation

Ce projet a été éxécuté durant l'année 2018 sur la période du module pendant les heures de TP et personnelles à la maison. \
Il a été terminé et rendu le 31/05/2018.

## Technologies, outils et procédés utilisés

Ce projet a été accomplis avec les technologies, outils et procédés suivants :
- Python ;
- SGBD SQLite ;
- Open Data ;
- Services Web (HTTP – REST – JSON) ;
- Flask ;
- JQuery ;
- Ajax.

## Objectifs

Les objectifs sont :
- Trouver des equipements sprotifs qui correspondent à une activité dans un périmètre donné ;
- Vérifier si ces équipements sont accessibles aux personnes à mobilité réduite ;
- Vérifier si on peut accéder aux équipements en transport en commun.

#! /usr/bin/python
# -*- coding:utf-8 -*-

from flask import *

"""
<p> Autheur : MAHAUDA Théo, DEGREZ Clara </p>
<p> Date : 30/05/2018 </p>
<p> Version : 1.0 </p>

<h4> Classe qui permet d'afficher les erreurs </h4>
"""
class ErreurImage(Exception):

    def __init__(self,message):
        self.message = message

    def __str__(self):
        return self.message

"""
<p> Autheur : MAHAUDA Théo, DEGREZ Clara </p>
<p> Date : 30/05/2018 </p>
<p> Version : 1.0 </p>

<h4> Classe qui permet de chercher des images </h4>
"""

class Image:

    def getImage(self):
        """
        Sélécteur qui permet de récupèrer le routeur Image

        :return: Le routeur Image
        :rtype: routeur
        """
        return self.image

    def __init__(self):
        """
        Constructeur qui permet de récupérer une image
        """
        self.image = Blueprint('image', __name__)

        @self.image.route('/rechercheImagesDiapo', methods=['GET'])
        def rechercheImagesDiapo():
            """
            Méthode qui permet de récupérer une image pour le diaporama du formulaire

            :param sport: Le code du sport choisi
            :type sport: string
            :return: L'image jpg
            :rtype: jpg
            """
            try:
                sport = request.args['sport']
                chemin = "app/controleur/css_js_images/images/diaporama/" + sport + ".jpg"
                return send_file(chemin, mimetype='image/jpg')
            except FileNotFoundError as erreur:
                raise ErreurImage("L'image n'existe pas' : {0}".format(erreur))

        @self.image.route('/rechercheImages', methods=['GET'])
        def rechercheImages():
            """
            Méthode qui permet de récupérer l'icone de l'activité selon le code activité

            :param sport: Le code du sport choisi
            :type sport: string
            :return: L'image png
            :rtype: png
            """
            try:
                sport = request.args['sport']
                chemin = "app/controleur/css_js_images/images/icones/sport/" + sport + ".png"
                return send_file(chemin, mimetype='image/png')
            except FileNotFoundError as erreur:
                raise ErreurImage("L'image n'existe pas' : {0}".format(erreur))

        @self.image.route('/rechercheSports', methods=['GET'])
        def rechercheSports():
            """
            Méthode qui permet de récupérer l'image selon le code activité

            :param sport: Le code du sport choisi
            :type sport: string
            :return: L'image jpg
            :rtype: jpg
            """

            try:
                sport = request.args['sport']
                chemin = "app/controleur/css_js_images/images/sport/" + sport + ".jpg"
                return send_file(chemin, mimetype='image/jpg')
            except FileNotFoundError as erreur:
                raise ErreurImage("L'image n'existe pas' : {0}".format(erreur))

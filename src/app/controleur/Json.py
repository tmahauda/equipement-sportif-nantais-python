#! /usr/bin/python
# -*- coding:utf-8 -*-

from flask import *
import requests
import json
from app.modele.Activites import *
from app.modele.Code_Postaux import *
from app.modele.Installation import *

"""
<p> Autheur : MAHAUDA Théo, DEGREZ Clara </p>
<p> Date : 30/05/2018 </p>
<p> Version : 1.0 </p>

<h4> Classe qui permet de générer des Jsons pour les renvoyer aux requetes Ajax </h4>
"""
class Json:

    def getJson(self):
        """
        Sélécteur qui permet de récupèrer le routeur Json

        :return: Le routeur Json
        :rtype: routeur
        """

        return self.json

    def __init__(self):
        """
        Constructeur qui permet de switcher entre les différentes routes de json selon la requete émise par l'Ajax
        """

        self.json = Blueprint('json', __name__)

        @self.json.route('/rechercheCode_Postaux', methods=['GET'])
        def rechercheCode_Postaux():
            """
            Méthode qui permet de récupérer tous les codes postaux en faisant appel au modele
            Code_Postaux grâce à une méthode GET html pour générer une liste de data à travers un Json

            :return: Un fichier Json grâce aux données de Code_Postaux
            :rtype: json
            """

            code_postaux = Code_Postaux()
            donneesCode_Postaux = code_postaux.getCode_Postaux()
            code_postaux.fermer()
            list=[]
            for donneeCode_Postale in donneesCode_Postaux:
                list.append(donneeCode_Postale[0])
            dicoCode_Postaux = {'status' : 'ok', 'data' : list}
            return jsonify(dicoCode_Postaux)

        @self.json.route('/rechercheActivites', methods=['GET'])
        def rechercheActivites():
            """
            Méthode qui permet de récupérer tous les activités d'un code postale en faisant appel au modele
            Activites grâce à une méthode GET html pour générer une liste de data à travers un Json

            :param cp: Le code postale
            :type cp: string
            :return: Un fichier Json grâce aux données de Code_Postaux
            :rtype: json
            """

            cp = request.args.get('cp')
            activites = Activites()
            donneesActivites = activites.getActivites(cp)
            listeActivitesLib=[]
            listeActivitesCode=[]
            for donneeActivite in donneesActivites:
                listeActivitesLib.append(donneeActivite[0])
                listeActivitesCode.append(donneeActivite[1])
            dicoActivites = {'status' : 'ok', 'activite' : listeActivitesLib, 'code' : listeActivitesCode}
            return jsonify(dicoActivites)

        @self.json.route('/rechercheAllActivites', methods=['GET'])
        def rechercheAllActivites():
            """
            Méthode qui permet de récupérer tous les activités en faisant appel au modele
            Activites grâce à une méthode GET html pour générer une liste de data à travers un Json

            :return: Un fichier Json grâce aux données de Code_Postaux
            :rtype: json
            """

            activites = Activites()
            donneesActivites = activites.getAllActivites()
            activites.fermer()
            list=[]
            for donneeActivite in donneesActivites:
                list.append(donneeActivite[0])
            dicoActivites = {'status' : 'ok', 'data' : list}
            return jsonify(dicoActivites)

        @self.json.route('/rechercheDescription', methods=['GET'])
        def rechercheDescription():
            """
            Méthode qui permet de récupérer la description d'une activité en faisant appel au modele
            Activites grâce à une méthode GET html pour générer une liste de data à travers un Json

            :param activite: Le code de l'activité choisi
            :type activite: string
            :return: Un fichier Json grâce aux données de Code_Postaux
            :rtype: json
            """

            activite = request.args.get('activite')
            activites = Activites()
            donneesActivites = activites.getActiviteDescription(activite)
            activites.fermer()
            description=[]
            for donneeActivite in donneesActivites:
                description.append(donneeActivite[0])
            dicoActivites = {'status' : 'ok', 'description' : description}
            return jsonify(dicoActivites)

        @self.json.route('/rechercheInstallation', methods=['GET'])
        def rechercheInstallation():
            """
            Méthode qui permet de récupérer les informations des installations en faisant appel au tableau associatif
            Session[] défini dans la méthode resultat() de Routeur grâce à une méthode GET html pour générer
            une liste de data à travers un Json

            :param cp: Le code postale
            :type cp: string
            :param activite: Le code de l'activité
            :type activite: string
            :param handicap: Si recherche activité par accessiblité mobilité réduite
            :type handicap: string
            :param transport: Si recherche par proximité transport en commun
            :type transport: string
            :param restaurant: Si recherche par proximité restaurant
            :type restaurant: string
            :return: Un fichier Json grâce aux données de Installation
            :rtype: json
            """

            installation = Installation()
            donneesInstallation = installation.getInstallation(session['cp'],session['codeActivite'],session['handicap'],session['transport'],session['restaurant'])
            installation.fermer()

            longitude=[]
            latitude=[]
            transport=[]
            nom=[]
            numero=[]
            libelle=[]
            codePostaux=[]
            ville=[]

            if(len(donneesInstallation)==0):

                dicoInstallation = {'status' : 'ko'}

            else:

                for donneeInstallation in donneesInstallation:
                    url = 'http://open.tan.fr/ewp/arrets.json?latitude='+str(donneeInstallation[1])+'&longitude='+str(donneeInstallation[0])
                    page = requests.get(url)

                    if len(page.json()) == 0:
                        transport.append({"codeLieu": "Néant","distance": "Néant","libelle": "Néant","ligne": [{"numLigne": "Néant"}]})
                    else:
                        transport.append(page.json()[0]) #On prend l'arret de transport la plus proche, d'ou l'index 0

                    longitude.append(donneeInstallation[0])
                    latitude.append(donneeInstallation[1])
                    nom.append(donneeInstallation[2])
                    numero.append(donneeInstallation[3])
                    libelle.append(donneeInstallation[4])
                    codePostaux.append(donneeInstallation[5])
                    ville.append(donneeInstallation[6])

            dicoInstallation = {'status' : 'ok', 'codeActivite' : session['codeActivite'], 'longitude' : longitude, 'latitude' : latitude, 'transport' : transport, 'nom' : nom, 'numeroVoie' : numero, 'libelle' : libelle, 'codePostaux' : codePostaux, 'ville' : ville}
            return jsonify(dicoInstallation)

        @self.json.route('/rechercheTransport', methods=['GET'])
        def rechercheTransport():
            """
            Méthode qui permet de récupérer les informations sur les transports en commun de Loire Atlantique uniquement
            pour les mettres dans une liste de data à travers un Json

            :param latitude: La latitude du lieu du sport choisi
            :type cp: string
            :param longitude: La longitude du lieu du sport choisi
            :type activite: string
            :return: Un fichier Json grâce aux paramètres latitude et longitude
            :rtype: json
            """

            latitude = request.args.get('latitude')
            longitude = request.args.get('longitude')
            url = 'http://open.tan.fr/ewp/arrets.json?latitude='+latitude+'&longitude='+longitude
            page = requests.get(url)
            return jsonify(page.json())

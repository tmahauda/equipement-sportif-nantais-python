#! /usr/bin/python
# -*- coding:utf-8 -*-

from flask import *
from jinja2 import TemplateNotFound

"""
<p> Autheur : MAHAUDA Théo, DEGREZ Clara </p>
<p> Date : 30/05/2018 </p>
<p> Version : 1.0 </p>

<h4> Classe qui permet d'afficher les erreurs </h4>
"""
class ErreurRouteur(Exception):

    def __init__(self,message):
        self.message = message

    def __str__(self):
        return self.message

"""
<p> Autheur : MAHAUDA Théo, DEGREZ Clara </p>
<p> Date : 30/05/2018 </p>
<p> Version : 1.0 </p>

<h4> Classe qui permet de naviguer entre les pages HTML </h4>
"""
class Routeur:

    def getRouteur(self):
        """
        Sélécteur qui permet de récupèrer le routeur du site

        :return: Le routeur du site
        :rtype: routeur
        """

        return self.routeur

    def __init__(self):
        """
        Constructeur qui permet de switcher entre les différentes routes du site selon la requete émis par le client
        """

        self.routeur = Blueprint('routeur',__name__)

        @self.routeur.route('/accueil', methods=["GET"])
        def accueil():
            """
            Méthode qui permet de récupérer la page d'accueil si elle existe

            :return: La page d'acceuil
            :rtype: html
            """

            try:
                return render_template('Formulaire.html')
            except TemplateNotFound as erreur:
                raise ErreurRouteur("La page d'accueil n'a pu être ouverte : {0}".format(erreur))

        @self.routeur.route('/recherche', methods=["GET"])
        def recherche():
            """
            Méthode qui permet de récupérer la page de recherche

            :return: La page de recherche
            :rtype: html
            """

            try:
                return render_template('Formulaire.html')
            except TemplateNotFound as erreur:
                raise ErreurRouteur("La page de formulaire n'a pu être ouverte : {0}".format(erreur))

        @self.routeur.route('/recherche', methods=["POST"])
        def resultat():
            cp = request.form['cp']
            activite = request.form['act']
            handicap = request.form['handicap']
            restaurant = request.form['restaurant']

            session.clear()
            session['cp'] = request.form['cp']
            session['codeActivite'] = request.form['act']
            session['handicap'] = request.form['handicap']
            session['transport'] = request.form['transport']
            session['restaurant'] = request.form['restaurant']
            session.modified = True

            try:
                return render_template('Carte.html')
            except TemplateNotFound as erreur:
                raise ErreurRouteur("La page carte n'a pu être ouverte : {0}".format(erreur))

        @self.routeur.route('/actualite', methods=['GET'])
        def actualite():
            try:
                return render_template('Formulaire.html')
            except TemplateNotFound as erreur:
                raise ErreurRouteur("La page de formulaire n'a pu être ouverte : {0}".format(erreur))

        @self.routeur.route('/forum', methods=['GET'])
        def forum():
            try:
                return render_template('Formulaire.html')
            except TemplateNotFound as erreur:
                raise ErreurRouteur("La page de formulaire n'a pu être ouverte : {0}".format(erreur))

        @self.routeur.route('/a_propos', methods=['GET'])
        def a_propos():
            try:
                return render_template('Formulaire.html')
            except TemplateNotFound as erreur:
                raise ErreurRouteur("La page de formulaire n'a pu être ouverte : {0}".format(erreur))

        @self.routeur.route('/contact', methods=['GET'])
        def contact():
            try:
                return render_template('Formulaire.html')
            except TemplateNotFound as erreur:
                raise ErreurRouteur("La page de formulaire n'a pu être ouverte : {0}".format(erreur))

        @self.routeur.route('/mentions_legales', methods=['GET'])
        def mentions_legales():
            try:
                return render_template('Formulaire.html')
            except TemplateNotFound as erreur:
                raise ErreurRouteur("La page de formulaire n'a pu être ouverte : {0}".format(erreur))

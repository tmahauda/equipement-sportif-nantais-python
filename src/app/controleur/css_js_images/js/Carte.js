/**
 * Affiche le résultat de la recherche sur une carte Google Maps
 */

$(document).ready(function(){

  /**
   * On affiche la carte dans un bloc
   */

  //On démarre l'animation patienter...
  $("#carte").loading({
    overlay: $('#custom-overlay')
  });

  var zoneMarkers = new google.maps.LatLngBounds();
  var map = new google.maps.Map(document.getElementById('carte'),
  {
    zoom: 20,
    maxZoom: 20, //Evite d'avoir une carte trop zoomé sur un point
    mapTypeId: google.maps.MapTypeId.ROADMAP //Type ville
  });

  /**
   * On récupère les données installations selon les critères du formulaire
   * par requete ajax
   */
  var ajax = $.ajax(
  {
    dataType : "json",
    url : "/json/rechercheInstallation",
    type : "GET"
  });

  /**
   * Création des marqueurs d'installation sur la carte
   */
  ajax.done(
    function(response)
    {
      var markers=[];
      var infowindows=[];
      var zoneMarkers = new google.maps.LatLngBounds();

      /**
       * Pour chaque marqueur
       */
      for(var i = 0; i < response.longitude.length; i++)
      {
        /**
         * On change l'icone du marqueur
         */
        var image =
        {
          url: "/image/rechercheImages?sport="+response.codeActivite,
          scaledSize: new google.maps.Size(50, 50)
        }

        /**
         * On crée le marqueur selon la latitude et longitude et on le stocke dans un tableau
         */
        markers[i] = new google.maps.Marker(
        {
          position: new google.maps.LatLng(response.latitude[i], response.longitude[i]),
          map: map,
          icon: image
        });

        markers[i].index = i;

        /**
         * On crée une fenetre pour chaque marqueur qui contiendra :
         * - Le nom de l'installation
         * - L'adresse
         * - Le(s) transport(s) en commun le plus proche si trouvé à Nantes
         */
        infowindows[i] = new google.maps.InfoWindow(
        {
          content:"<h1>" + response.nom[i] + "</h1>"
                  +"<p>" + response.numeroVoie[i] + " " + response.libelle[i] + "</p>"
                  +"<p>" + response.codePostaux[i] + " " + response.ville[i] + "</p>"
                  +"<p> Ligne(s) : " + $.map(response.transport[i].ligne, function(ligne){return ligne.numLigne;}).join(" , ") + "</p>"
                  +"<p> Arrêt : " + response.transport[i].libelle + "</p>"
        });

        /**
         * On associe à chaque marqueur la fenetre.
         * Lors du clique sur un marqueur, on ouvre la fenetre
         */
        google.maps.event.addListener(markers[i], 'click', function()
        {
          //Si des fenetres sont déjà ouverte, alors on les fermes avant d'ouvrir l'autre
          for(var j=0; j<infowindows.length; j++)
          {
            infowindows[j].close()
          }

          infowindows[this.index].open(map,markers[this.index]);
          map.panTo(markers[this.index].getPosition());
        });

        //A chaque marqueur crée, on rédinis la zone
        zoneMarkers.extend(markers[i].getPosition());
      }
      //On centre la carte sur la zone de marqueurs
      map.fitBounds(zoneMarkers);

      //On arrete l'animation patienter...
      $("#carte").loading('stop');
    });
})

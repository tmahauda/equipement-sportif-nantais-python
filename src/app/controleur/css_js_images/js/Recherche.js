/**
 * Anime le menu de navigation
 */

function init()
{
  var menu = document.getElementsByClassName("menu");
  for(var i=0 ; i<menu.length ; i++)
  {
    menu[i].addEventListener("mouseover", cliquerOui, false);
    menu[i].addEventListener("mouseout", cliquerNon, false);
  }
}

function cliquerOui()
{
  this.style.backgroundColor = "#0081C8";
  this.style.color = "white";
}

function cliquerNon()
{
  this.style.backgroundColor = "#04A7FF";
  this.style.color = "black";
}

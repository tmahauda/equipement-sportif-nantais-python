import sqlite3

"""
<p> Autheur : MAHAUDA Théo, DEGREZ Clara </p>
<p> Date : 30/05/2018 </p>
<p> Version : 1.0 </p>

<h4> Classe qui permet de récupérer les informations des activités sportives Pays de la Loire </h4>
"""
class Activites:

    def __init__(self):
        """
        Constructeur qui permet d'initialiser la base de donnée pour effectuer des requetes
        """

        self.connection = sqlite3.connect("Equipements_sportifs/src/app/modele/donnees/sources/Equipements_sportifs.db")
        self.requete = self.connection.cursor()

    def fermer(self):
        self.connection.close()

    def getActivites(self,code_postale):
        """
        Méthode qui permet de récupèrer toutes les activités pour un code postale donnée

        :param code_postale: Le code postale de l'activité
        :type code_postale: string
        :return: Toutes les activités pour le code postale donnée
        :rtype: list[string]
        """

        projections = "SELECT DISTINCT afe.ActLib, afe.ActCode"
        tables = " FROM Fiches_Equipements fe, Fiches_Installations fi, Activites_Fiches_Equipements afe"
        conditionCP = " WHERE fi.InsCodePostal = ?"
        conditionJOIN1 = " AND afe.ComInsee = fe.ComInsee"
        conditionJOIN2 = " AND afe.EquipementId = fe.EquipementId"
        conditionJOIN3 = " AND fe.ComInsee = fi.ComInsee"
        conditionJOIN4 = " AND fe.InsNumeroInstall = fi.InsNumeroInstall"
        ordonnees = " ORDER BY 1"

        requeteActivites = projections + tables + conditionCP + conditionJOIN1 + conditionJOIN2 + conditionJOIN3 + conditionJOIN4 + ordonnees
        self.requete.execute(requeteActivites, (code_postale,))

        return self.requete.fetchall()

    def getAllActivites(self):
        """
        Méthode qui permet de récupèrer toutes les activités Pays de la Loire

        :return: Toutes les activités pour le code postale donnée
        :rtype: list[string]
        """

        requeteActivites = "SELECT DISTINCT afe.ActLib FROM Activites_Fiches_Equipements afe WHERE afe.ActLib NOT IN ('', 'ActLib') ORDER BY 1"
        self.requete.execute(requeteActivites)
        return self.requete.fetchall()

    def getActiviteDescription(self,codeActivite):
        """
        Méthode qui permet de récupèrer la description d'une activité donnée

        :param codeActivite: L'activité
        :type codeActivite: string
        :return: La description de l'activité
        :rtype: string
        """

        requeteActivites = "SELECT DISTINCT ad.ActDescrip FROM Activites_Description ad WHERE ad.ActCode = ? ORDER BY 1"
        self.requete.execute(requeteActivites, (codeActivite,))
        return self.requete.fetchall()

import sqlite3

"""
<p> Autheur : MAHAUDA Théo, DEGREZ Clara </p>
<p> Date : 30/05/2018 </p>
<p> Version : 1.0 </p>

<h4> Classe qui permet de récupérer tous les codes postaux Pays de la Loire </h4>
"""
class Code_Postaux:

    def __init__(self):
        """
        Constructeur qui permet d'initialiser la base de donnée pour effectuer des requetes
        """

        self.connection = sqlite3.connect("Equipements_sportifs/src/app/modele/donnees/sources/Equipements_sportifs.db")
        self.requete = self.connection.cursor()

    def fermer(self):
        self.connection.close()

    def getCode_Postaux(self):
        """
        Méthode qui permet de récupèrer tous les codes postaux Pays de la Loire

        :return: Toutes les codes postaux
        :rtype: list[string]
        """

        requeteCode_Postaux = "SELECT DISTINCT fi.InsCodePostal FROM Fiches_Installations fi WHERE fi.InsCodePostal NOT IN ('Code postal', '') ORDER BY CAST(fi.InsCodePostal AS INT)"
        self.requete.execute(requeteCode_Postaux)
        return self.requete.fetchall()

import sqlite3

"""
<p> Autheur : MAHAUDA Théo, DEGREZ Clara </p>
<p> Date : 30/05/2018 </p>
<p> Version : 1.0 </p>

<h4> Classe qui permet de récupérer toutes les informations relatives aux installations Pays de la Loire </h4>
"""

class Installation:

    def __init__(self):
        """
        Constructeur qui permet d'initialiser la base de donnée pour effectuer des requetes
        """

        self.connection = sqlite3.connect("Equipements_sportifs/src/app/modele/donnees/sources/Equipements_sportifs.db")
        self.requete = self.connection.cursor()

    def fermer(self):
        self.connection.close()

    def getInstallation(self,cp,codeActivite,handicap,transport,restaurant):
        """
        Méthode qui permet de récupèrer toutes les installations pour un code postale, une activité, un handicap, un transport et un restaurant données

        :param cp: Le code postale de l'activité
        :param codeActivite: Le code activité considérée
        :param handicap: Si la personne veut une activité accessible à l'handicap
        :param transport: Si la personne veut une activité proche d'un transport en commun
        :param restaurant: Si la personne veut une activité proche d'un resto
        :type cp: string
        :type codeActivite: string
        :type handicap: string
        :type transport: string
        :type restaurant: string
        :return: Toutes les coordonnées, l'adresse de l'activité pour un code postale donnée
        :rtype: list[string]
        """

        conditionHAND = ""
        conditionREST = ""
        conditionTRANS = ""

        if(handicap == "true"):
            print("Handicap oui")
            conditionHAND = " AND fi.InsAccessibiliteHandiMoteur = 'Oui'"

        if(transport == "true"):
            print("Transport oui")
            conditionTRANS = " AND (fi.InsTransportBus = 'Oui' OR fi.InsTransportTram = 'Oui' OR fi.InsTransportMetro = 'Oui')"

        if(restaurant == "true"):
            print("Restaurant oui")
            conditionREST = " AND fe.EquErpN = ''"

        projections = "SELECT DISTINCT fe.EquGpsX, fe.EquGpsY, fe.InsNom, fi.InsNoVoie, fi.InsLibelleVoie, fi.InsCodePostal, fi.ComLib"
        tables = " FROM Fiches_Equipements fe, Fiches_Installations fi, Activites_Fiches_Equipements afe"
        conditionCP = " WHERE fi.InsCodePostal = ?"
        conditionACT = " AND afe.ActCode = ?"
        conditionJOIN1 = " AND afe.ComInsee = fe.ComInsee"
        conditionJOIN2 = " AND afe.EquipementId = fe.EquipementId"
        conditionJOIN3 = " AND fe.ComInsee = fi.ComInsee"
        conditionJOIN4 = " AND fe.InsNumeroInstall = fi.InsNumeroInstall"

        requeteInstallation = projections + tables + conditionCP + conditionACT + conditionJOIN1 + conditionJOIN2 + conditionJOIN3 + conditionJOIN4 + conditionHAND + conditionTRANS + conditionREST
        self.requete.execute(requeteInstallation, (cp,codeActivite))

        return self.requete.fetchall()

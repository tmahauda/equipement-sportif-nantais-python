#!/usr/bin/python3
#-*- coding: utf-8 -*-

import sqlite3

class ErreurRequete(Exception):

    def __init__(self,message):
        self.message = message

    def __str__(self):
        return self.message

class BD:

    def __init__(self):
        self.connection = sqlite3.connect("Equipements_sportifs/src/app/modele/donnees/sources/Equipements_sportifs.db")
        self.requete = self.connection.cursor()

    def fermer(self):
        self.connection.close()

    def remplirDonneesFichesEquipements(self,donnees):
        try:
            donneesTriees = []
            for ligne in donnees:
                donneesTriees.append((ligne[0], ligne[1], ligne[2], ligne[3], ligne[4], ligne[5], ligne[20], ligne[179], ligne[180]))

            self.requete.execute("DROP TABLE Fiches_Equipements")
            self.requete.execute("CREATE TABLE IF NOT EXISTS Fiches_Equipements(ComInsee TEXT, ComLib TEXT, InsNumeroInstall TEXT, InsNom TEXT, EquipementId TEXT, EquNom TEXT, EquErpN TEXT, EquGpsX FLOAT, EquGpsY FLOAT)")
            self.requete.executemany("INSERT INTO Fiches_Equipements VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)", donneesTriees)
            self.connection.commit()

        except sqlite3.OperationalError as erreur:
            raise ErreurRequete("La requête est mal formé : {0}".format(erreur))

    def remplirDonneesFichesInstallations(self,donnees):
        try:
            donneesTriees = []
            for ligne in donnees:
                if len(ligne[4]) == 5:
                    donneesTriees.append((ligne[0], ligne[1], ligne[2], ligne[3], ligne[4], ligne[5], ligne[6], ligne[7], ligne[12], ligne[22], ligne[23], ligne[24], ligne[25]))

            self.requete.execute("DROP TABLE Fiches_Installations")
            self.requete.execute("CREATE TABLE IF NOT EXISTS Fiches_Installations(InsNom TEXT, InsNumeroInstall TEXT, ComLib TEXT, ComInsee TEXT, InsCodePostal TEXT, InsLieuDit TEXT, InsNoVoie TEXT, InsLibelleVoie TEXT, InsAccessibiliteHandiMoteur TEXT, InsTransportMetro TEXT, InsTransportBus TEXT, InsTransportTram TEXT, InsTransportTrain TEXT)")
            self.requete.executemany("INSERT INTO Fiches_Installations VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", donneesTriees)
            self.connection.commit()

        except sqlite3.OperationalError as erreur:
            raise ErreurRequete("La requête est mal formé : {0}".format(erreur))

    def remplirDonneesActivitesFichesEquipements(self,donnees):
        try:
            donneesTriees = []
            for ligne in donnees:
                #Seul une partie du nom de l'activité nous intéresse, les commentaires après / ou () nous interesse pas
                ligne[5] = ligne[5].split('/')[0]
                ligne[5] = ligne[5].split('(')[0]
                ligne[5] = ligne[5].strip()
                donneesTriees.append((ligne[0], ligne[1], ligne[2], ligne[3], ligne[4], ligne[5], ligne[6], ligne[7], ligne[8], ligne[9]))

            self.requete.execute("DROP TABLE Activites_Fiches_Equipements")
            self.requete.execute("CREATE TABLE IF NOT EXISTS Activites_Fiches_Equipements(ComInsee TEXT, ComLib TEXT, EquipementId TEXT, EquNbEquIdentique TEXT, ActCode TEXT, ActLib TEXT, EquActivitePraticable TEXT, EquActivitePratique TEXT, EquActiviteSalleSpe TEXT, ActNivLib TEXT)")
            self.requete.executemany("INSERT INTO Activites_Fiches_Equipements VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", donneesTriees)
            self.connection.commit()

        except sqlite3.OperationalError as erreur:
            raise ErreurRequete("La requête est mal formé : {0}".format(erreur))

    def remplirDonneesActivitesDescription(self,donnees):
        try:
            donneesTriees = []
            for ligne in donnees:
                donneesTriees.append((ligne[0], ligne[1]))

            self.requete.execute("DROP TABLE Activites_Description")
            self.requete.execute("CREATE TABLE IF NOT EXISTS Activites_Description(ActCode TEXT, ActDescrip TEXT)")
            self.requete.executemany("INSERT INTO Activites_Description VALUES(?, ?)", donneesTriees)
            self.connection.commit()

        except sqlite3.OperationalError as erreur:
            raise ErreurRequete("La requête est mal formé : {0}".format(erreur))

    def recupererDonnees(self, requete):
        try:
            self.requete.execute(requete)
            return self.requete.fetchall()

        except sqlite3.OperationalError as erreur:
            raise ErreurRequete("La requête est mal formé : {0}".format(erreur))

#!/usr/bin/python3
#-*- coding: utf-8 -*-

import csv
import os
from urllib.request import urlretrieve

class ErreurOuverture(Exception):

    def __init__(self,message):
        self.message = message

    def __str__(self):
        return self.message

class ErreurTelechargement(Exception):

    def __init__(self,message):
        self.message = message

    def __str__(self):
        return self.message

class Fichier:

    def telecharger(self,url,chemin):
        os.remove(chemin)
        urlretrieve(url,chemin)

    def ouvrir(self,chemin,encodage):
        try:
            self.fichier = open(chemin, "r", encoding=encodage)
        except IOError as erreur:
            raise ErreurOuverture("Le fichier n'a pu être ouvert : {0}".format(erreur))

    def lire(self,delimiteur):
        return csv.reader(self.fichier, delimiter=delimiteur)

    def fermer(self):
        self.fichier.close()

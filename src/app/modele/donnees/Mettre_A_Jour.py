#!/usr/bin/python3
#-*- coding: utf-8 -*-

from Fichier import *
from BD import *

try:
    """ On récupère les données des fichiers csv"""

    """ Les fiches equipements """
    urlFichesEquipements = "http://data.paysdelaloire.fr/fileadmin/data/datastore/rpdl/sport/23440003400026_J336/equipements.csv"
    cheminFichesEquipements = "Equipements_sportifs/src/app/modele/donnees/sources/Fiches_equipements.csv"
    fichierFichesEquipements = Fichier()
    #fichierFichesEquipements.telecharger(urlFichesEquipements,cheminFichesEquipements)
    fichierFichesEquipements.ouvrir(cheminFichesEquipements,"latin-1")
    donneesFichesEquipements = fichierFichesEquipements.lire(";")

    """ Les fiches installations """
    urlFichesInstallations = "http://data.paysdelaloire.fr/api/publication/23440003400026_J335/installations_table/content/?format=csv"
    cheminFichesInstallations = "Equipements_sportifs/src/app/modele/donnees/sources/Fiches_installations.csv"
    fichierFichesInstallations = Fichier()
    #fichierFichesInstallations.telecharger(urlFichesInstallations,cheminFichesInstallations)
    fichierFichesInstallations.ouvrir(cheminFichesInstallations, "utf-8")
    donneesFichesInstallations = fichierFichesInstallations.lire(",")

    """ Activités des fiches équipements """
    urlActivitesFichesEquipements = "http://data.paysdelaloire.fr/fileadmin/data/datastore/pdl/PLUS15000/J334_equipements_activites.csv"
    cheminActivitesFichesEquipements = "Equipements_sportifs/src/app/modele/donnees/sources/Activites_fiches_equipements.csv"
    fichierActivitesFichesEquipements = Fichier()
    #fichierActivitesFichesEquipements.telecharger(urlActivitesFichesEquipements,cheminActivitesFichesEquipements)
    fichierActivitesFichesEquipements.ouvrir(cheminActivitesFichesEquipements, "utf-8")
    donneesActivitesFichesEquipements = fichierActivitesFichesEquipements.lire(",")

    """ Activités description """
    cheminActivitesDescription = "Equipements_sportifs/src/app/modele/donnees/sources/Activites_description.csv"
    fichierActivitesDescription = Fichier()
    #fichierActivitesFichesEquipements.telecharger(urlActivitesFichesEquipements,cheminActivitesFichesEquipements)
    fichierActivitesDescription.ouvrir(cheminActivitesDescription, "utf-8")
    donneesActivitesDescription = fichierActivitesDescription.lire(";")

    """ On remplie la base avec les données """
    bd = BD()
    bd.remplirDonneesFichesEquipements(donneesFichesEquipements)
    bd.remplirDonneesFichesInstallations(donneesFichesInstallations)
    bd.remplirDonneesActivitesFichesEquipements(donneesActivitesFichesEquipements)
    bd.remplirDonneesActivitesDescription(donneesActivitesDescription)

    requete = "SELECT DISTINCT ActLib, ActCode FROM Activites_Fiches_Equipements ORDER BY 1"
    donnees = bd.recupererDonnees(requete)

    for donnee in donnees:
        print(donnee)

except ErreurOuverture as erreur:
    print(erreur)
except ErreurRequete as erreur:
    print(erreur)

finally:
    fichierFichesEquipements.fermer()
    fichierFichesInstallations.fermer()
    fichierActivitesFichesEquipements.fermer()
    bd.fermer()

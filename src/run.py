#! /usr/bin/python
# -*- coding:utf-8 -*-

from flask import *
from app.controleur.Routeur import *
from app.controleur.Json import *
from app.controleur.Image import *

"""
<p> Autheur : MAHAUDA Théo, DEGREZ Clara </p>
<p> Date : 30/05/2018 </p>
<p> Version : 1.0 </p>

<h4> Permet de démarrer l'application en préparant les différents routeurs : </h4>
<h4> - Le routeur du site permettant de naviguer </h4>
<h4> - Le routeur json pour renvoyer les données du serveur au client </h4>
<h4> - Le routeur image pour renvoyer les images du serveur au client </h4>
"""

routeur = Routeur()
json = Json()
image = Image()

app = Flask(__name__, static_folder="app/controleur/css_js_images", template_folder="app/vue/html")
app.secret_key = "super secret key"
app.config['JSON_AS_ASCII'] = False
app.register_blueprint(routeur.getRouteur(), url_prefix='/site')
app.register_blueprint(json.getJson(), url_prefix='/json')
app.register_blueprint(image.getImage(), url_prefix='/image')

@app.route('/', methods=["GET"])
def index():
    return redirect(url_for('routeur.recherche'))

if __name__ == '__main__':
    app.run(debug=True, use_reloader=True)
